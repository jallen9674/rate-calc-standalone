<html>
    <head>
        <title>Veteran's Disability Calculator | John Allen</title>
        <meta type="description" content="A quick calculator to determine how much disability benefits veterans are owed">
        <meta type="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width">

        <link type="text/css" rel="stylesheet" href="./css/normalize.css"/>
        <link type="text/css" rel="stylesheet" href="./css/styles.css"/>

        <script src="./js/vendor/modernizr-3.5.0.min.js"></script>
    </head>

    <body>

        <div class="view-code">
            <a href="https://bitbucket.org/jallen9674/rate-calc-standalone/src/master/" target="_blank"><img alt="View Source" src="./images/bitbucket-icon.svg"> View Source on Bitbucket</a>
        </div>
        <div class="portfolio-return">
            <a href="https://jsallen.com/portfolio/">&laquo; Return to Portfolio</a>
        </div>


        <div class="calc-demo">
            <?php require("./partials/disability-calculator.php"); ?>
        </div>

        <div class="view-code">
            <a href="https://bitbucket.org/jallen9674/rate-calc-standalone/src/master/" target="_blank"><img alt="View Source" src="./images/bitbucket-icon.svg"> View Source on Bitbucket</a>
        </div>
        <div class="portfolio-return">
            <a href="https://jsallen.com/portfolio/">&laquo; Return to Portfolio</a>
        </div>



        <script src="./js/vendor/jquery-3.2.1.min.js"></script>       
        <script src="./js/rate-calculation.js"></script>
        <script src="./js/main.js"></script>

    </body>

</html>