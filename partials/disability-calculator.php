<?php //BEGIN APP ?>

<div class="rate-calculator-outer">
<div class="rate-calculator-inner-wrapper">
<h2 class="rate-calculator-title">Veterans Disability Calculator</h2>
<div class="rate-calculator-top">
    <div class="rate-calculator-top__inner">

        <div class="rate-calculator-top__left">

            <h2 class="rate-calculator-top__disability-percentage-title">Current Disability Percentage</h2>
            <div class="rate-calculator-top__disability-percentage">
                <div class="rate-calculator-top__disability-percentage-inner">
                    <span class="current-disability-percentage" id="current-disability-percentage">0</span>%
                </div>
            </div>

            <div class="rate-calculator-top__bilateral-info">
                <h3 class="rate-calculator-top__bilateral-info-text bilateral-arm-info-text">Bilateral Arm Factor Applied: <span class="current-bilat-arm-factor" id="current-bilat-arm-factor">0</span></h3>
                <h3 class="rate-calculator-top__bilateral-info-text bilateral-leg-info-text">Bilateral Leg Factor Applied: <span class="current-bilat-leg-factor" id="current-bilat-leg-factor">0</span></h3>
            </div>

        </div>

        <div class="rate-calculator-top__right">
            <h2>Add Disabilities</h2>
            <div class="rate-calculator-top__bilateral-injuries">
                <p>If your disability involves an arm or leg, select the appropriate extremity and the disability rating. If your disability does not involve an extremity, just select the disability rating.</p>
                <div class="bilateral-calulator-buttons">
                    <button class="bilateral-calculator-buttons__single-button" data-bilateral-location="LL">Left Leg</button>
                    <button class="bilateral-calculator-buttons__single-button" data-bilateral-location="RL">Right Leg</button>
                    <button class="bilateral-calculator-buttons__single-button" data-bilateral-location="LA">Left Arm</button>
                    <button class="bilateral-calculator-buttons__single-button" data-bilateral-location="RA">Right Arm</button>
                </div>
            </div> <?php //End bilateral-injuries ?>

            <div class="rate-calculator-top__add-disabilities">
                <div class="disability-calculator-buttons">
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="10">10%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="20">20%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="30">30%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="40">40%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="50">50%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="60">60%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="70">70%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="80">80%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="90">90%</button>
                    <button class="disability-calculator-buttons__single-button" data-disability-percent="100">100%</button>
                </div>
            </div> <?php //End add-disabilities ?>

            <div class="rate-calculator-top__disabilities-added">
                <h2>Current Disabilities Applied</h2>
                <div>
                    <div class="current-disabilities-added-row" id="current-disabilities-added-row">
                        <span class="current-disabilities-added-row__filler-text">No Disabilities Added Yet</span>
                    </div>
                </div>
            </div> <?php //End disabilities-added ?>

        </div>

    </div>
</div> <?php //End .rate-calculator-top ?>


<div class="rate-calculator-middle">
    <div class="rate-calculator-middle__inner-row">
        <h2 class="rate-calculator-middle__row-title">Additional Payment Amount Factors</h2>

        <div class="rate-calculator-middle__left">
            <div class="rate-calculator-middle__dependent-information">
                <p>Number of Dependent Children Under 18 Years Old</p>
                <div class="custom-select">
                    <select class="dependent-children-18-select">
                        <option value="" disabled selected>Select a number</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div> <?php //End dependent-information ?>
        </div> <?php //End Left ?>

        <div class="rate-calculator-middle__right">
            <p>Number of Dependent Children Between 18 and 24 Years Old</p>
            <div class="custom-select">
                <select class="dependent-children-24-select">
                    <option value="" disabled selected>Select a number</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div> <?php //End Right ?>

    </div> <?php //End inner-row-1 ?>

    <div class="rate-calculator-middle__inner-row">

        <div class="rate-calculator-middle__left">
            <p>Number of Dependent Parents</p>
            <div class="custom-select">
                <select class="dependent-parents-select">
                    <option value="" disabled selected>Select a number</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
            </div>
        </div> <?php //End Left ?>

        <div class="rate-calculator-middle__right">
            <div class="rate-calculator-middle__marital-status">

                <p>Marital Status</p>
                <div class="marital-status-wrapper">
                    <div class="marital-status">
                        <button data-marital-status="0" class="marital-status__single-button marital-status__single marriage-button-active">Single</button>
                        <button data-marital-status="1" class="marital-status__single-button marital-status__married">Married</button>
                    </div>
                </div>

                <div class="aid-and-attendance-status-wrapper aa-status-disabled">
                    <p>Does your spouse need Aid and Attendance(A/A)?</p>
                    <div class="aid-and-attendance-status">
                        <button data-aa-status="0" class="aid-and-attendance-status__single-button aa-status__no aa-button-active">No</button>
                        <button data-aa-status="1" class="aid-and-attendance-status__single-button aa-status__yes">Yes</button>
                    </div>
                </div>

            </div> <?php //End marital-status ?>
        </div> <?php //End Right ?>

    </div> <?php //End inner-row ?>

</div> <?php //End rate-calculator-middle ?>


<div class="rate-calculator-bottom">
    <div class="rate-calculator-bottom__inner">

        <div class="rate-calculator-bottom__left">
            <h2 class="rate-calculator-bottom__disability-rating-title">Current Disability Rating</h2>
            <div class="rate-calculator-bottom__disability-rating">
                <div class="rate-calculator-bottom__disability-rating-inner">
                    <span class="final-disability-percentage" id="final-disability-percentage">0</span>%</h2>
                </div>
            </div>
        </div> <?php //End bottom-left ?>

        <div class="rate-calculator-bottom__right">
            <div class="rate-calculator-bottom__payment-amount">
                <h2 class="rate-calculator-bottom__payment-amount-title">Your Monthly Payment Amount</h2>
                <h3 class="rate-calculator-bottom__payment-amount-text">$<span class="payment-display">0</span></h3>
            </div> <?php //End payment-amount ?>
        </div> <?php //End bottom-right ?>


    <div class="rate-calculator-reset-row">

        <div class="reset-button-wrapper">
            <button class="reset-calculator reset-calculator-button" id="reset-calculator">Reset All</button>
        </div>

    </div> <?php //End .rate-calculator-reset-row ?>

    </div>
</div> <?php //End .rate-calculator-bottom ?>

</div> <?php //End .rate-calculator-inner-wrapper ?>
</div> <?php //End .rate-calculator-outer ?>



<?php //END APP ?>
