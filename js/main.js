/*--------------------------------
Adding Disability Notifications to Array and DOM
--------------------------------*/

$( document ).ready(function() {

    var disabilityArray = [];
    var bilateralArmArray = [];
    var bilateralLegArray = [];
    var finalDisabilityRating;
    var bilateralArmPercentage;
    var bilateralLegPercentage;
    var bilateralLeftArmArray = [];
    var bilateralRightArmArray = [];
    var bilateralLeftLegArray = [];
    var bilateralRightLegArray = [];
    var finalRoundedDisabilityRating;

    //Store Values to Be Removed from Array on Bilat Calc -- NEED TO IMPLEMENT
    //Then on successful calc, remove these 2 from main array and replace with new value, then recalc
    var bilatRightArmPercent;
    var bilatLeftArmPercent;
    var bilatRightLegPercent = 0;
    var bilatLeftLegPercent = 0;


    //Storing Factors that Influence Disability Rating
    var dependentChildren18 = 0;
    var dependentChildren24 = 0;
    var hasChildren = 0;
    var maritalStatus = 0; //0 Single, 1 Married
    var dependentParent = 0;
    var aaStatus = 0;


    //For Array Sorting
    function sortNumber(a,b) {
        return a - b;
    }

    //Alternate Reset Button
    $('.reset-calculator').on('click', function(element){
            //Reset Behind the Scenes
            disabilityArray = [];
            bilateralArmArray = [];
            bilateralLegArray = [];
            finalDisabilityRating = 0;
            bilatRightArmPercent = 0;
            bilatLeftArmPercent = 0;
            bilatRightLegPercent = 0;
            bilatLeftLegPercent = 0;
            bilateralFactor = 0;
            disabilityRating = 0;
            bilateralArmPercentage = undefined;
            bilateralLegPercentage = undefined;
            dependentChildren18 = 0;
            dependentChildren24 = 0;
            dependentParent = 0;
            maritalStatus = 0;
            aaStatus = 0;
            hasChildren = 0;
            bilateralLeftArmArray = [];
            bilateralRightArmArray = [];
            bilateralLeftLegArray = [];
            bilateralRightLegArray = [];

            //Reset DOM
            $('.current-disability-percentage').html('0');
            $('.final-disability-percentage').html('0');
            $('.current-bilat-arm-disability-percentage').html('0%');
            $('.current-bilat-leg-disability-percentage').html('0%');
            $('.current-disabilities-added-row').empty();
            $('.bilateral-calculator-buttons__single-button').removeClass('button-disabled');
            $('.bilateral-calculator-buttons__single-button').removeClass('bilateral-active');
            $('.current-bilat-arm-factor').html('0');
            $('.current-bilat-leg-factor').html('0');
            $('.payment-display').html('0');
            $('.current-disabilities-added-row').html('<span class="current-disabilities-added-row__filler-text">No Disabilities Added Yet</span>');

            //Reset Spousal Items
            $('.aa-status__yes').removeClass('aa-button-active');
            $('.aa-status__no').addClass('aa-button-active');
            $('.marital-status__single').addClass('marriage-button-active');
            $('.marital-status__married').removeClass('marriage-button-active');
            $('.aid-and-attendance-status-wrapper').addClass('aa-status-disabled');

            //Reset Select Dropdowns
            $(".dependent-children-18-select").val($(".dependent-children-18-select option:first").val());
            $(".dependent-children-24-select").val($(".dependent-children-24-select option:first").val());
            $(".dependent-parents-select").val($(".dependent-parents-select option:first").val());

    });

    //Remove Disability and Recalulate Score
    /* Functionality Disabled Due to Time Constraints -- NEEDS WORK
    $(document).on('click', '.remove-current-disability', function (element) {

        var initialDisabilityPercent = $(this).parent().attr('data-disability-percent');
        console.log('Value Removed: ' + initialDisabilityPercent);

        //Find Index of Element with Value and Remove
        var index = disabilityArray.indexOf(initialDisabilityPercent);
        if (index > -1) {
            disabilityArray.splice(index, 1);
        }

        //If BiLateral Is Removed Re-enable Exteremity Button -- Working

        var bilateralAttribute = $(this).parent().attr('data-bilateral-status');
        if (typeof bilateralAttribute !== typeof undefined && bilateralAttribute !== false) {

          // Element has bilateral attribute, So Renable Button
          $('.bilateral-calculator-buttons__single-button').each(function(){
             if ( $(this).attr('data-bilateral-location') ==  bilateralAttribute ) {
                $(this).removeClass('button-disabled');
             }
          });

            //TO DO REMOVE BILAT VALUE FROM BILAT ARRAY
            //Find Index of Element with Value and Remove

            //Pick Value to Remove
            var bilatArmPercentToRemove;
            if (bilateralAttribute == 'LA') {
                bilatArmPercentToRemove = bilatLeftArmPercent;
                console.log('bilatLeftArmPercentToRemove - ' + bilatArmPercentToRemove );
            };
            if (bilateralAttribute == 'RA') {
                bilatArmPercentToRemove = bilatRightArmPercent;
                console.log('bilatLeftArmPercentToRemove - ' + bilatArmPercentToRemove );
            };

            //Remove Value & Reset Arm Percentage
            var biLatArmindex = bilateralArmArray.indexOf(bilatArmPercentToRemove);
            if (biLatArmindex > -1) {
                bilateralArmArray.splice(biLatArmindex, 1);
                $('.current-bilat-arm-disability-percentage').html('0%');
                console.log('Value Removed: ' + bilatArmPercentToRemove);
                console.log('BiLat Array: ' + bilateralArmArray);

                //Add Remaining BiLat Value to Main Array
                disabilityArray.push(bilateralArmArray[0]);
                console.log('Value Added ' + bilateralArmArray[0]);
            }

            //Reset Storage Variables if Removed
            if (bilateralAttribute == 'LA') {
                bilatLeftArmPercent = 'null';
                console.log('bilatLeftArmPercent' + bilatLeftArmPercent );
            };
            if (bilateralAttribute == 'RA') {
                bilatRightArmPercent =  'null';
                console.log('bilatRightArmPercent' + bilatRightArmPercent );
            };

        }


        //Remove Current Item from DOM -- Working
        $(this).parent().remove();

        //Recalculate if array isn't empty
        if (disabilityArray.length > 0) {
            calculateRating($(this));
        } else {
            //Remove Value From DOM
            $('.current-disability-percentage').html('0%');
        }


    }); */

    //Update DOM on Button Press
    $('.disability-calculator-buttons__single-button').on('click', function() {

        //Grab Bilateral Location if Selected and Clear Button Status
        var biLateralLocation = $('.bilateral-active').attr('data-bilateral-location');
        $('.bilateral-active').removeClass('bilateral-active');
        console.log(biLateralLocation);

        //If Bilateral is Selected Store in Seperate Array for Additional Calculation
        //ARMS
        if (biLateralLocation == 'LA' || biLateralLocation == 'RA'){
            //addNewBilateralArmValueToDisabilityArray($(this));

            //Store Percentages If Needed Later for Recalc
            if (biLateralLocation == 'LA') {
                bilateralLeftArmArray.push($(this).attr('data-disability-percent'));
                console.log('Bilateral Left Arm Array: ' + bilateralLeftArmArray );
            };

            if (biLateralLocation == 'RA') {
                bilateralRightArmArray.push($(this).attr('data-disability-percent'));
                console.log('Bilateral Right Arm Array: ' + bilateralRightArmArray );
            };


            //Perform Calc
            if ( (bilateralLeftArmArray.length >= 1) && (bilateralRightArmArray.length >= 1) ){
                //If Both Perform BiLateral Calc
                console.log('Arm Bilateral Calculation Active');
                calculateBiLateralArmRating(bilateralLeftArmArray,bilateralRightArmArray);  //Working
                addRatingToDOM($(this), biLateralLocation);
            } else {
                //Otherwise Calculate as normal
                addNewValueToDisabilityArray($(this));
                calculateRating($(this)); //Working
                addRatingToDOM($(this), biLateralLocation); //Working
            }

        } else
        //LEGS
        if (biLateralLocation == 'LL' || biLateralLocation == 'RL'){
            //addNewBilateralLegValueToDisabilityArray($(this));

            //Store Percentages If Needed Later for Recalc
            if (biLateralLocation == 'LL') {
                bilateralLeftLegArray.push($(this).attr('data-disability-percent'));
                console.log('Bilateral Left Leg Array: ' + bilateralLeftLegArray );
            };

            if (biLateralLocation == 'RL') {
                bilateralRightLegArray.push($(this).attr('data-disability-percent'));
                console.log('Bilateral Right Leg Array: ' + bilateralRightLegArray );
            };


            //Perform Calc
            if ( (bilateralLeftLegArray.length >= 1) && (bilateralRightLegArray.length >= 1) ){
                //If Both Perform BiLateral Calc
                console.log('Leg Bilateral Calculation Active');
                calculateBiLateralLegRating(bilateralLeftLegArray, bilateralRightLegArray);  //Working
                addRatingToDOM($(this), biLateralLocation);
            } else {
                //Otherwise Calculate as normal
                addNewValueToDisabilityArray($(this));
                calculateRating($(this)); //Working
                addRatingToDOM($(this), biLateralLocation); //Working
            }

        } else {
            addNewValueToDisabilityArray($(this));
            calculateRating($(this));
            addRatingToDOM($(this));
        }


    });


    //Add Styles to Bilateral Button on Click
    $('.bilateral-calculator-buttons__single-button').on('click', function(){

        if ( $(this).hasClass('bilateral-active') ) {
            $(this).removeClass('bilateral-active');
        } else {
            $('.bilateral-calculator-buttons__single-button').removeClass('bilateral-active');
            $(this).addClass('bilateral-active');
        }

    });


    //Calculate Disability Estimate for 1 Disabilities

    function calculateDisabilityPercentageSingle(a, b){

        var disabilityOne = parseInt(a);
        var disabilityPercentage1 = disabilityOne;
        return disabilityPercentage1;

    }


    //Calculate Disability Estimate for 2 Disabilities

    function calculateDisabilityPercentageMultiple(a, b){


        var capableOne = 100 - parseInt(a); //Percentage Capable

        var disabilityOne = parseInt(a); //Percentage Capable
        var disabilityTwo = 0.01*parseInt(b); //Percentage Disabled

        var intermediateDisabilityNum = capableOne*disabilityTwo;
        var currentDisability = disabilityOne + intermediateDisabilityNum;

        if (currentDisability >= 95) {
            //Stupid VA Rounding
            return 100;
        } else {
            return currentDisability;
        }

    }

    function addRatingToDOM(element, bilateral) {
        var initialDisabilityPercent = $(element).attr('data-disability-percent');

        //Checking to See if Bilateral is Defined
        bilateral = (typeof bilateral !== 'undefined') ?  bilateral : '';

        //Output Value of Disability to DOM
        var disabilityPercentageList = document.getElementById('current-disabilities-added-row');
        var bilateralText;

        switch (bilateral) {
            case 'LL':
                bilateralText = ' Left Leg';
            break;
            case 'RL':
                bilateralText = ' Right Leg';
            break;
            case 'LA':
                bilateralText = ' Left Arm';
            break;
            case 'RA':
                bilateralText = ' Right Arm';
            break;
        default:
                bilateralText = '';
        }

        $('.current-disabilities-added-row__filler-text').remove();
        var singleDisabilityPercentageOutput = document.createElement('div');
          singleDisabilityPercentageOutput.innerHTML  = '<span class="current-disabilities-added" data-disability-percent="' + initialDisabilityPercent + '" data-bilateral-status="' + bilateral + '">'
                                                      + initialDisabilityPercent + '%' + bilateralText + '</span>';

        while (singleDisabilityPercentageOutput.firstChild) {
            disabilityPercentageList.appendChild(singleDisabilityPercentageOutput.firstChild);
        }
    }

    //Adding Values to Arrays
    //Non-Bilateral Array
    function addNewValueToDisabilityArray(element) {
        var initialDisabilityPercent = $(element).attr('data-disability-percent');

        //Add Value of Disability to Array
        disabilityArray.push(initialDisabilityPercent);
        console.log('Pre-sort: ' + disabilityArray);
        disabilityArray.sort(sortNumber).reverse();
        console.log('Post-sort: ' + disabilityArray);
    }


    //On Addition Or Removal of Disability Recalculate Score
    function calculateRating(element){

        //Perform Rate Calculation For 3 or More
        if (disabilityArray.length > 2) {

            //Loop Through Array For Each Entry
            console.log('calculateRating > 3 Started');
            finalDisabilityRating = 0;
            for (i = 1; (i + 1) < disabilityArray.length + 1; i++) {

                if ( i > 1 ) {
                    var intDisability = finalDisabilityRating;
                    var disabilityRating = calculateDisabilityPercentageMultiple(intDisability, disabilityArray[i]);
                    finalDisabilityRating = Math.round(disabilityRating);
                    console.log('DR in calculateRating: ' + disabilityRating + ' | Rounded: ' + finalDisabilityRating);
                    $('.current-disability-percentage').html(finalDisabilityRating);

                    //Rounding to Nearest 10 for Actual Rate Calculations

                    $('.final-disability-percentage').html((Math.round(finalDisabilityRating / 10) * 10));
                    var roundedDisabilityRating = (Math.round(finalDisabilityRating / 10) * 10)
                    finalRoundedDisabilityRating = roundedDisabilityRating;
                    updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);

                } else {
                    var disabilityRating = calculateDisabilityPercentageMultiple(disabilityArray[i-1], disabilityArray[i]);
                    console.log('Multiple3: ' + disabilityRating);
                    finalDisabilityRating = Math.round(disabilityRating);
                    $('.current-disability-percentage').html(finalDisabilityRating);

                    //Rounding to Nearest 10 for Actual Rate Calculations

                    $('.final-disability-percentage').html((Math.round(finalDisabilityRating / 10) * 10));
                    var roundedDisabilityRating = (Math.round(finalDisabilityRating / 10) * 10);
                    finalRoundedDisabilityRating = roundedDisabilityRating;
                    updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);
                }

                console.log('Stored Final DR (' + (i + 1) + ' M): ' + finalDisabilityRating);
            }
        //Perform Rate Calculation For 2
        } else if (disabilityArray.length > 1) {
            var disabilityRating = calculateDisabilityPercentageMultiple(disabilityArray[0], disabilityArray[1]);
            finalDisabilityRating = Math.round(disabilityRating);
            $('.current-disability-percentage').html(finalDisabilityRating);

            //Rounding to Nearest 10 for Actual Rate Calculations

            $('.final-disability-percentage').html((Math.round(finalDisabilityRating / 10) * 10));
            var roundedDisabilityRating = (Math.round(finalDisabilityRating / 10) * 10);
            finalRoundedDisabilityRating = roundedDisabilityRating;
            updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);

        } else {
            //Perform Rate Calculation For 1
            var disabilityRating = calculateDisabilityPercentageSingle(disabilityArray[0], disabilityArray[1]);
            finalDisabilityRating = Math.round(disabilityRating);
             $('.current-disability-percentage').html(finalDisabilityRating);

            //Rounding to Nearest 10 for Actual Rate Calculations
            $('.final-disability-percentage').html((Math.round(finalDisabilityRating / 10) * 10));
            var roundedDisabilityRating = (Math.round(finalDisabilityRating / 10) * 10);
            finalRoundedDisabilityRating = roundedDisabilityRating;
            updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);
        }
    }

    //On Addition Or Removal of Disability Recalculate Score
    //Note Changes Here Need to be Duplicated in the Leg Rating Below
    function calculateBiLateralArmRating(leftarm, rightarm){

         var bilateralDisabilityRating;
         var combinedArmArray = leftarm.concat(rightarm);

         //Sort Combined Array
         combinedArmArray.sort(sortNumber).reverse();
         console.log('Sorted Combined Arm Array: ' + combinedArmArray);


         if (combinedArmArray.length >= 2) {

            //Get Value of First Bilat Disability Added
            var intermediateArray = [];

            //Store Non bilateral disabilities in array --- NOTE WILL NEED TO MODIFY TO TAKE INTO ACCOUNT LEG CALCS
            $('.current-disabilities-added').each(function(){

                if (  (bilateralRightLegArray.length == 0 ) || (bilateralLeftLegArray.length == 0) ) {
                    console.log('No Bilateral Leg Effect Active');
                    if ( ($(this).attr('data-bilateral-status') !== 'LA') && ($(this).attr('data-bilateral-status') !== 'RA') ) {
                        intermediateArray.push($(this).attr('data-disability-percent'));
                        console.log('IntArray: ' + intermediateArray);
                    }
                }

                if ( ( (bilateralRightLegArray.length >= 1) && (bilateralLeftLegArray.length >= 1) ) ) {
                    console.log('Bilateral Leg Effect Active');
                    if ( ($(this).attr('data-bilateral-status') !== 'LA') && ($(this).attr('data-bilateral-status') !== 'RA') && ($(this).attr('data-bilateral-status') !== 'LL') && ($(this).attr('data-bilateral-status') !== 'RL') ) {
                        intermediateArray.push($(this).attr('data-disability-percent'));
                        console.log('IntArrayNoLeg: ' + intermediateArray);
                    }
                }

            }); //End Each

            //If Leg Bilateral Calculation Active Add Their Value to Array
            if ( bilateralLegPercentage != undefined  ){
                intermediateArray.push(bilateralLegPercentage);
                console.log('Initial Arm Int Array: ' + intermediateArray + 'Initial Leg Percentage: ' + bilateralLegPercentage);
            }

            //Calculate Bilateral Disability Rating (Without Bilateral Factor)
            var disabilityRating = calculateBilateralPercentageRating(combinedArmArray);
            bilateralDisabilityRating = disabilityRating;
            console.log('Raw Bilateral Arm Rating:  ' + disabilityRating);

            //Calculate & Rounds Bilateral Factor
            var bilateralFactor = disabilityRating*.1;

            bilateralDisabilityRating = bilateralFactor + disabilityRating;
            //Round to Nearest Whole Number
            bilateralDisabilityRating = Math.round(bilateralDisabilityRating);

            bilateralArmPercentage = bilateralDisabilityRating;

            console.log('Arm BiLateral Factor: ' + bilateralFactor + '| Arm Disability Total: ' + bilateralDisabilityRating);

            //$('.current-bilat-arm-disability-percentage').html(bilateralDisabilityRating + '%');

            //Round BiLateral Factor and Display
            bilateralFactor = bilateralFactor.toFixed(2);
            $('.current-bilat-arm-factor').html(bilateralFactor);


            //Replace Main Disability Array with Intermediate Array and add Bilateral Rating
            disabilityArray.length = 0;
            disabilityArray.push.apply(disabilityArray, intermediateArray);
            disabilityArray.push(bilateralDisabilityRating);

            //Resort Disability Array Large to Small
            disabilityArray.sort(sortNumber).reverse();
            //disabilityArray.shift();




            //Need to get rid of starting whitespace
            console.log('Current Arm Intermediate Array After Removal: ' + intermediateArray + ' | LENGTH ' + intermediateArray.length ); //Wprking to here
            console.log('Current Arm Disability Array After Removal: ' + disabilityArray + ' | LENGTH ' + disabilityArray.length ); //Wprking to here

            //Recalulate Main Disability Rating
            calculateRating();

        }
    } //End Arm Rating Calc

     //On Addition Or Removal of Disability Recalculate Score
    function calculateBiLateralLegRating(leftleg, rightleg){

         var bilateralDisabilityRating;
         var combinedLegArray = leftleg.concat(rightleg);

         //Sort Combined Array
         combinedLegArray.sort(sortNumber).reverse();
         console.log('Sorted Combined Leg Array: ' + combinedLegArray);

         if (combinedLegArray.length >= 2) {


            //Get Value of First Bilat Disability Added
            var intermediateArray = [];
             $('.current-disabilities-added').each(function(){
                //Store Non bilateral disabilities in array --- NOTE WILL NEED TO MODIFY TO TAKE INTO ACCOUNT LEG CALCS
                 if (  (bilateralRightArmArray.length == 0 ) || (bilateralLeftArmArray.length == 0) ) {
                    console.log('No Bilateral Arm Effect Active');
                    if ( ($(this).attr('data-bilateral-status') !== 'LL') && ($(this).attr('data-bilateral-status') !== 'RL') ) {
                        intermediateArray.push($(this).attr('data-disability-percent'));
                        console.log('IntArray: ' + intermediateArray);
                    }
                }

                if ( (bilateralRightArmArray.length >= 1) && (bilateralLeftArmArray.length >= 1) ) {
                    console.log('Bilateral Leg Effect Active');
                    if ( ($(this).attr('data-bilateral-status') !== 'LA') && ($(this).attr('data-bilateral-status') !== 'RA') && ($(this).attr('data-bilateral-status') !== 'LL') && ($(this).attr('data-bilateral-status') !== 'RL') ) {
                        intermediateArray.push($(this).attr('data-disability-percent'));
                        console.log('IntArrayNoArm: ' + intermediateArray);
                    }
                }
            });

            //If 2 Arms Exist Add their Value to Array
            if ( bilateralArmPercentage != undefined  ){
                intermediateArray.push(bilateralArmPercentage);
                console.log('Initial Arm Int Array: ' + intermediateArray + 'Initial Leg Percentage: ' + bilateralArmPercentage);
            }


            //Calculate Bilateral Disability Rating (Without Bilateral Factor)
            var disabilityRating = calculateBilateralPercentageRating(combinedLegArray);
            bilateralDisabilityRating = disabilityRating;
            console.log('Raw Leg Bilateral Percentage: ' + disabilityRating);

            //Calculate & Rounds Bilateral Factor
            var bilateralFactor = disabilityRating*.1;

            bilateralDisabilityRating = bilateralFactor + disabilityRating;
            //Round to Nearest Whole Number
            bilateralDisabilityRating = Math.round(bilateralDisabilityRating);

            bilateralLegPercentage = bilateralDisabilityRating;

            console.log('Leg BiLateral Factor: ' + bilateralFactor + '| Leg Disability Total: ' + bilateralDisabilityRating);

            //$('.current-bilat-leg-disability-percentage').html(bilateralDisabilityRating + '%');

            //Round BiLateral Factor and Display
            bilateralFactor = bilateralFactor.toFixed(2);
            $('.current-bilat-leg-factor').html(bilateralFactor);


            //Replace Main Disability Array with Intermediate Array and add Bilateral Rating
            disabilityArray.length = 0;
            disabilityArray.push.apply(disabilityArray, intermediateArray);
            disabilityArray.push(bilateralDisabilityRating);

            //Resort Disability Array Large to Small
            disabilityArray.sort(sortNumber).reverse();
            //disabilityArray.shift();


            //Need to get rid of starting whitespace

            console.log('Current Leg Disability Array After Removal: ' + disabilityArray + 'LENGTH ' + disabilityArray.length ); //Wprking to here

            //Recalulate Main Disability Rating
            calculateRating();

        }
    }


//On Addition Or Removal of Disability Recalculate Score
function calculateBilateralPercentageRating(array){
    var bilateralCalculatedPercentage;

    //Perform Rate Calculation For 3 or More
    if (array.length > 2) {

        //Loop Through Array For Each Entry
        console.log('Bilateral Percentage Rating > 3 Calc Active');
        finalDisabilityRating = 0;

        for (i = 1; (i + 1) < array.length + 1; i++) {

            if ( i > 1 ) {
                var intDisability = bilateralCalculatedPercentage;
                var disabilityRating = calculateDisabilityPercentageMultiple(intDisability, array[i]);
                bilateralCalculatedPercentage = Math.round(disabilityRating);
            } else {
                var disabilityRating = calculateDisabilityPercentageMultiple(array[i-1], array[i]);
                bilateralCalculatedPercentage = Math.round(disabilityRating);
            }

        }
    //Perform Rate Calculation For 2
    } else if (array.length > 1) {
        console.log('Bilateral Percentage Rating 2 Calc Active');
        var disabilityRating = calculateDisabilityPercentageMultiple(array[0], array[1]);
        bilateralCalculatedPercentage = Math.round(disabilityRating);
    }

    console.log('Bilateral Percentage Calc Complete: ' + bilateralCalculatedPercentage);
    return bilateralCalculatedPercentage;
}



/*--------------------------------
Adding Event Listeners to Dependent/Married Buttons
--------------------------------*/

//Dependent Children Under 18
$('.dependent-children-18-select').change(function(){
    dependentChildren18 = $(this).val();
    hasChildren = 1;

    if ( (dependentChildren18 == 0) && (dependentChildren24 == 0) ) {
        hasChildren = 0;
    }

    console.log('Dependent Children 18 Updated: ' + dependentChildren18);
    updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);
});

//Dependent Children Between 18 and 24
$('.dependent-children-24-select').change(function(){
    dependentChildren24 = $(this).val();
    hasChildren = 1;

    if ( (dependentChildren18 == 0) && (dependentChildren24 == 0) ) {
        hasChildren = 0;
    }

    console.log('Dependent Children 24 Updated: ' + dependentChildren24);
    updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);
});

//Dependent Parents
$('.dependent-parents-select').change(function(){
    dependentParent = $(this).val();
    console.log('Dependent Parent Updated: ' + dependentParent);
    updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);
});

//Marital Status
$('.marital-status__single').on('click', function(){

    if ( !$(this).hasClass('marriage-button-active') ) {
        $('.marriage-button-active').removeClass('marriage-button-active');
        $(this).addClass('marriage-button-active');

        maritalStatus = $(this).attr('data-marital-status');
        console.log('Marital Status Updated: ' + maritalStatus);
        updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);

        //Hide Aid and Attendance Option and Reset Value
        $('.aid-and-attendance-status-wrapper').addClass('aa-status-disabled');
        $('.aa-status__yes').removeClass('aa-button-active');
        $('.aa-status__no').addClass('aa-button-active');
        aaStatus = 0;
    }
});

$('.marital-status__married').on('click', function(){

    if ( !$(this).hasClass('marriage-button-active') ) {
        $('.marriage-button-active').removeClass('marriage-button-active');
        $(this).addClass('marriage-button-active');

        maritalStatus = $(this).attr('data-marital-status');
        console.log('Marital Status Updated: ' + maritalStatus);
        updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);

        //Display Aid and Attendance Option
        $('.aid-and-attendance-status-wrapper').removeClass('aa-status-disabled');
    }

});


//Add and Attendance Status (A/A)

$('.aa-status__yes').on('click', function(){

    if ( !$(this).hasClass('aa-button-active') ) {
        $('.aa-button-active').removeClass('aa-button-active');
        $(this).addClass('aa-button-active');

        aaStatus = $(this).attr('data-aa-status');
        console.log('AA Status Updated: ' + aaStatus);
        updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);

    }

});

$('.aa-status__no').on('click', function(){

    if ( !$(this).hasClass('aa-button-active') ) {
        $('.aa-button-active').removeClass('aa-button-active');
        $(this).addClass('aa-button-active');

        aaStatus = $(this).attr('data-aa-status');
        console.log('AA Status Updated: ' + aaStatus);
        updatePaymentAmount(dependentChildren18, dependentChildren24, hasChildren, maritalStatus, dependentParent, aaStatus, finalRoundedDisabilityRating);

    }

});


}); //End Document Ready