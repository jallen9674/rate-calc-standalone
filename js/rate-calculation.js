/***************************************************
Rate Calulations Based on Final Disability Rating
/**************************************************/

//Get Status of Veteran
//finalDisabilityRating


//dc18: Dependent Children Under 18
//dc24: Dependent Children Between 18 and 24
//ms: Marital Status
//dp: Dependent Parents
//dr: Disability Rating
//aa: Additional Care for Parent
//hc: Has Any Children

function updatePaymentAmount(dc18, dc24, hc, ms, dp, aa, dr) {

    var monthlyPayment;

    console.log('Payment Update Triggered');

    /*-------------------------------------------------------
    **  Single Veteran w/ No Dependents
    -------------------------------------------------------*/

    if ( (dc18 == 0) && (dc24 == 0) && (ms == 0) && (dp == 0) ) {
        console.log('Single Vet no Dependents Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 408.97;
            break;
            case 40:
                monthlyPayment = 589.12;
            break;
            case 50:
                monthlyPayment = 838.64;
            break;
            case 60:
                monthlyPayment = 1062.27;
            break;
            case 70:
                monthlyPayment = 1338.71;
            break;
            case 80:
                monthlyPayment = 1556.13;
            break;
            case 90:
                monthlyPayment = 1748.71;
            break;
            case 100:
                monthlyPayment = 2915.55;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Single Veteran w/ No Dependents

    /*-------------------------------------------------------
    **  Veteran with Spouse & No Children
    -------------------------------------------------------*/

    if ( (dc18 == 0) && (dc24 == 0) && (ms == 1) && (dp == 0) ) {
        console.log('Veteran with Spouse & No Children Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 456.97;
            break;
            case 40:
                monthlyPayment = 654.12;
            break;
            case 50:
                monthlyPayment = 919.64;
            break;
            case 60:
                monthlyPayment = 1159.27;
            break;
            case 70:
                monthlyPayment = 1451.71;
            break;
            case 80:
                monthlyPayment = 1686.13;
            break;
            case 90:
                monthlyPayment = 1894.71;
            break;
            case 100:
                monthlyPayment = 3078.11;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse & No Children

    /*-------------------------------------------------------
    **  Veteran with Spouse, No Children, And 1 Parent
    -------------------------------------------------------*/

    if ( (dc18 == 0) && (dc24 == 0) && (ms == 1) && (dp == 1) ) {
        console.log('Veteran with Spouse, No Children & 1 Parent Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 495.97;
            break;
            case 40:
                monthlyPayment = 706.12;
            break;
            case 50:
                monthlyPayment = 984.64;
            break;
            case 60:
                monthlyPayment = 1237.27;
            break;
            case 70:
                monthlyPayment = 1542.71;
            break;
            case 80:
                monthlyPayment = 1790.13;
            break;
            case 90:
                monthlyPayment = 2011.71;
            break;
            case 100:
                monthlyPayment = 3208.56;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse, No Children, And 1 Parent

    /*-------------------------------------------------------
    **  Veteran with Spouse, No Children, And 2 Parents
    -------------------------------------------------------*/

    if ( (dc18 == 0) && (dc24 == 0) && (ms == 1) && (dp == 2) ) {
        console.log('Veteran with Spouse, No Children & 2 Parents Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 534.97;
            break;
            case 40:
                monthlyPayment = 758.12;
            break;
            case 50:
                monthlyPayment = 1049.64;
            break;
            case 60:
                monthlyPayment = 1315.27;
            break;
            case 70:
                monthlyPayment = 1633.71;
            break;
            case 80:
                monthlyPayment = 1894.13;
            break;
            case 90:
                monthlyPayment = 2128.71;
            break;
            case 100:
                monthlyPayment = 3339.01;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse, No Children, And 2 Parents

    /*-------------------------------------------------------
    **  Single Veteran with No Children and 1 Parent
    -------------------------------------------------------*/

    if ( (dc18 == 0) && (dc24 == 0) && (ms == 0) && (dp == 1) ) {
        console.log('Single Veteran with No Children and 1 Parent Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 447.97;
            break;
            case 40:
                monthlyPayment = 641.12;
            break;
            case 50:
                monthlyPayment = 903.64;
            break;
            case 60:
                monthlyPayment = 1140.27;
            break;
            case 70:
                monthlyPayment = 1429.71;
            break;
            case 80:
                monthlyPayment = 1660.13;
            break;
            case 90:
                monthlyPayment = 1865.71;
            break;
            case 100:
                monthlyPayment = 3046.00;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Single Veteran with No Children and 1 Parent

    /*-------------------------------------------------------
    **  Single Veteran with No Children and 2 Parents
    -------------------------------------------------------*/

    if ( (dc18 == 0) && (dc24 == 0) && (ms == 0) && (dp == 2) ) {
        console.log('Single Veteran with No Children and 2 Parents Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 486.97;
            break;
            case 40:
                monthlyPayment = 693.12;
            break;
            case 50:
                monthlyPayment = 968.64;
            break;
            case 60:
                monthlyPayment = 1218.27;
            break;
            case 70:
                monthlyPayment = 1520.71;
            break;
            case 80:
                monthlyPayment = 1764.13;
            break;
            case 90:
                monthlyPayment = 1982.71;
            break;
            case 100:
                monthlyPayment = 3176.45;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Single Veteran with No Children and 2 Parents


    /*-------------------------------------------------------
    **  Veteran with Spouse and Child
    -------------------------------------------------------*/

    if ( (hc == 1) && (ms == 1) && (dp == 0) ) {
        console.log('Veteran with Spouse and at Least One Child Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 492.97;
            break;
            case 40:
                monthlyPayment = 702.12;
            break;
            case 50:
                monthlyPayment = 978.64;
            break;
            case 60:
                monthlyPayment = 1230.27;
            break;
            case 70:
                monthlyPayment = 1534.71;
            break;
            case 80:
                monthlyPayment = 1781.13;
            break;
            case 90:
                monthlyPayment = 2001.71;
            break;
            case 100:
                monthlyPayment = 3197.16;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse and Child

    /*-------------------------------------------------------
    **  Veteran with Child Only
    -------------------------------------------------------*/

    if ( (hc == 1) && (ms == 0) && (dp == 0) ) {
        console.log('Veteran with at Least One Child Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 440.97;
            break;
            case 40:
                monthlyPayment = 632.12;
            break;
            case 50:
                monthlyPayment = 892.64;
            break;
            case 60:
                monthlyPayment = 1127.27;
            break;
            case 70:
                monthlyPayment = 1414.71;
            break;
            case 80:
                monthlyPayment = 1642.13;
            break;
            case 90:
                monthlyPayment = 1845.71;
            break;
            case 100:
                monthlyPayment = 3024.27;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse and Child

    /*-------------------------------------------------------
    **  Veteran with Spouse, Child, and 1 Parent
    -------------------------------------------------------*/

    if ( (hc == 1) && (ms == 1) && (dp == 1) ) {
        console.log('Veteran with Spouse, Child, and 1 Parent Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 531.97;
            break;
            case 40:
                monthlyPayment = 754.12;
            break;
            case 50:
                monthlyPayment = 1043.64;
            break;
            case 60:
                monthlyPayment = 1308.27;
            break;
            case 70:
                monthlyPayment = 1625.71;
            break;
            case 80:
                monthlyPayment = 1885.13;
            break;
            case 90:
                monthlyPayment = 2118.71;
            break;
            case 100:
                monthlyPayment = 3327.61;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse, Child, and 1 Parent

    /*-------------------------------------------------------
    **  Veteran with Spouse, Child, and 2 Parents
    -------------------------------------------------------*/

    if ( (hc == 1) && (ms == 1) && (dp == 2) ) {
        console.log('Veteran with Spouse, Child, and 2 Parents Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 570.97;
            break;
            case 40:
                monthlyPayment = 806.12;
            break;
            case 50:
                monthlyPayment = 1108.64;
            break;
            case 60:
                monthlyPayment = 1386.27;
            break;
            case 70:
                monthlyPayment = 1716.71;
            break;
            case 80:
                monthlyPayment = 1989.13;
            break;
            case 90:
                monthlyPayment = 2235.71;
            break;
            case 100:
                monthlyPayment = 3458.06;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Veteran with Spouse, Child, and 2 Parents

    /*-------------------------------------------------------
    **  Single Veteran with Child, and 1 Parent
    -------------------------------------------------------*/

    if ( (hc == 1) && (ms == 0) && (dp == 1) ) {
        console.log('Single Veteran with Child, and 1 Parent Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 479.97;
            break;
            case 40:
                monthlyPayment = 684.12;
            break;
            case 50:
                monthlyPayment = 957.64;
            break;
            case 60:
                monthlyPayment = 1205.27;
            break;
            case 70:
                monthlyPayment = 1505.71;
            break;
            case 80:
                monthlyPayment = 1745.13;
            break;
            case 90:
                monthlyPayment = 1962.71;
            break;
            case 100:
                monthlyPayment = 3154.72;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Single Veteran with Child, and 1 Parent

    /*-------------------------------------------------------
    **  Single Veteran with Child, and 2 Parents
    -------------------------------------------------------*/

    if ( (hc == 1) && (ms == 0) && (dp == 2) ) {
        console.log('Single Veteran with Child, and 2 Parents Pricing Table Used');

        switch (dr) {
            case 10:
                monthlyPayment = 133.57;
            break;
            case 20:
                monthlyPayment = 264.02;
            break;
            case 30:
                monthlyPayment = 518.97;
            break;
            case 40:
                monthlyPayment = 736.12;
            break;
            case 50:
                monthlyPayment = 1022.64;
            break;
            case 60:
                monthlyPayment = 1283.27;
            break;
            case 70:
                monthlyPayment = 1596.71;
            break;
            case 80:
                monthlyPayment = 1850.13;
            break;
            case 90:
                monthlyPayment = 2079.71;
            break;
            case 100:
                monthlyPayment = 3285.17;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End Single Veteran with Child, and 2 Parents




    /*-------------------------------------------------------
    **  Additional Bonus for Married Veteran With A/A Spouse
    -------------------------------------------------------*/

     if ( (ms == 1) && (aa == 1) ) {
        console.log('A/A Payment Increase Applied');

        switch (dr) {
            case 10:
                monthlyPayment = monthlyPayment + 0;
            break;
            case 20:
                monthlyPayment = monthlyPayment + 0;
            break;
            case 30:
                monthlyPayment = monthlyPayment + 45;
            break;
            case 40:
                monthlyPayment = monthlyPayment + 59;
            break;
            case 50:
                monthlyPayment = monthlyPayment + 74;
            break;
            case 60:
                monthlyPayment = monthlyPayment + 89;
            break;
            case 70:
                monthlyPayment = monthlyPayment + 105;
            break;
            case 80:
                monthlyPayment = monthlyPayment + 119;
            break;
            case 90:
                monthlyPayment = monthlyPayment + 134;
            break;
            case 100:
                monthlyPayment = monthlyPayment + 149.08;
            break;
        default:
                monthlyPayment = 0;
        }

    } //End A/A Payment Increase

    /*-------------------------------------------------------
    **  Additional Bonuses for More than One Child
    -------------------------------------------------------*/

    if ( (hc == 1)  ){

        var totalChildren = dc18 + dc24;
        var childBonus = 0;
        var childrenUnder18 = dc18;
        var childrenUnder24 = dc24;

        console.log('Children Under 18: ' + childrenUnder18);
        console.log('Children Over 18: ' + childrenUnder24);

        if ( (childrenUnder24 == 0) && (childrenUnder18 > 1) ){
            //Means No Children Over 18
            console.log('No children over 18');
            childrenUnder18 = childrenUnder18 - 1;

            childBonus = calculateChildBonus(childrenUnder18, childrenUnder24, dr);

        } //End If All Children Under 18

        if ( (childrenUnder18 == 0) && (childrenUnder24 > 1) ) {
            //Means No Children Over 24
            console.log('No children over 24');
            childrenUnder24 = childrenUnder24 - 1;
            childBonus = calculateChildBonus(childrenUnder18, childrenUnder24, dr);
        }

        if ( (childrenUnder24 > 0 ) && (childrenUnder18 > 0 ) && (childrenUnder18 !== 1) && (childrenUnder24 !== 1)) {
            //A mix of children
            console.log('Mixed Age Children');
            childrenUnder18 = childrenUnder18 - 1;
            childrenUnder24 = childrenUnder24;
            childBonus = calculateChildBonus(childrenUnder18, childrenUnder24, dr);
        }

        //Update Monthly Payment
        monthlyPayment = childBonus + monthlyPayment;

    } //End Extra Children Bonuses


    //Update DOM
    updateDisplayedPaymentAmount(monthlyPayment);
}


//Update Payment Amount On Page
function updateDisplayedPaymentAmount(amount) {
    $('.payment-display').html(amount.toFixed(2));
    console.log('Payment Amount Updated: ' + amount.toFixed(2));
}

//Calculate Child Bonus

function calculateChildBonus(under18, over18, disability){

    var childBonusAmountTotal = 0;
    var childBonusAmountUnder18 = 0;
    var childBonusAmountOver18 = 0;
    console.log('calulateChildBonus active');

     //Under 18 Child Bonus Calculation
    if ( under18 > 0 ){
        console.log('Calculating Child Bonus for Under 18');
        switch (disability) {
            case 30:
               childBonusAmountUnder18 = (under18)*24;
            break;
            case 40:
               childBonusAmountUnder18 = (under18)*32;
            break;
            case 50:
               childBonusAmountUnder18 = (under18)*40;
            break;
            case 60:
               childBonusAmountUnder18 = (under18)*48;
            break;
            case 70:
               childBonusAmountUnder18 = (under18)*56;
            break;
            case 80:
               childBonusAmountUnder18 = (under18)*64;
            break;
            case 90:
               childBonusAmountUnder18 = (under18)*72;
            break;
            case 100:
               childBonusAmountUnder18 = (under18)*80.76;
            break;
        default:
                childBonusAmountUnder18 = childBonusAmountUnder18;
        } //End below 18 bonus

        console.log('Children Under 18 Used for Bonus: ' + under18);
        console.log('Children Under 18 Bonus Amount: ' + childBonusAmountUnder18);
    } //End if

    //Between 18 and 24 Child Bonus Calculation
    if (over18 > 0 ) {
        console.log('Calculating Child Bonus for Over 18');
        switch (disability) {
            case 30:
               childBonusAmountOver18 = (over18)*78;
            break;
            case 40:
               childBonusAmountOver18 = (over18)*104;
            break;
            case 50:
               childBonusAmountOver18 = (over18)*130;
            break;
            case 60:
               childBonusAmountOver18 = (over18)*156;
            break;
            case 70:
               childBonusAmountOver18 = (over18)*182;
            break;
            case 80:
               childBonusAmountOver18 = (over18)*208;
            break;
            case 90:
               childBonusAmountOver18 = (over18)*234;
            break;
            case 100:
               childBonusAmountOver18 = (over18)*260.91;
            break;
        default:
                childBonusAmountOver18 = childBonusAmountOver18;
        } //End 18-24 Bonus

        console.log('Children Over 18 Used for Bonus: ' + over18);
        console.log('Children Over 18 Bonus Amount: ' + childBonusAmountOver18);
    } //End if

    childBonusAmount = childBonusAmountOver18 + childBonusAmountUnder18;

    return childBonusAmount;
}